Summary:        Icinga Build test package
License:        GPLv2+
Group:          System/Monitoring
Name:           icingabuildtest
Version:        1.0.0
Release:        1%{?dist}
Url:            https://www.icinga.com
Source:         https://github.com/Icinga/%{name}/archive/v%{version}.tar.gz

BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

Requires:       bash

%description
This is just a test package.

%prep
%setup -q -n %{name}-%{version}

%build
bash icingabuildtest

%install
install -d %{buildroot}%{_bindir}
install -m 0755 icingabuildtest %{buildroot}%{_bindir}/icingabuildtest

%files
%defattr(-,root,root,-)
%{_bindir}/icingabuildtest

%changelog
* Thu Feb 07 2019 Markus Frosch <markus.frosch@icinga.com> 1.0.0-1
- Initial package
